import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class GridService {
  constructor(private http: HttpClient) {}

  getServiceData() {
    return this.http.get("./assets/data.json");
  }
}
