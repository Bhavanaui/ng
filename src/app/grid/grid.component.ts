import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-grid",
  templateUrl: "./grid.component.html",
  styleUrls: ["./grid.component.scss"]
})
export class GridComponent implements OnInit {
  @Input() isPaginatable: boolean;
  @Input() data: any;
  @Input() pageSize: number;
  currentPage: number = 0;
  totalPages: number = 0;
  headerColumns = [];
  displayData: any = [];

  constructor() {}

  ngOnInit() {
    this.initializeData();
    this.headerColumns = Object.keys(this.data[0]);
  }

  initializeData() {
    this.totalPages = Math.ceil(this.data.length / this.pageSize);
    this.displayData = this.data.slice(0, this.pageSize);
  }

  next() {
    this.currentPage++;
    if (this.currentPage < this.totalPages) {
      let count = this.currentPage * this.pageSize;
      this.displayData = this.data.slice(count, count + this.pageSize);
    } else {
      this.currentPage--;
    }
  }

  prev() {
    this.currentPage--;
    if (this.currentPage >= 0) {
      let count = this.currentPage * this.pageSize;
      this.displayData = this.data.slice(count, count + this.pageSize);
    } else {
      this.currentPage++;
    }
  }

  changePageSize() {
    let node: any = document.getElementById("grid_page_size");
    let size = node.value;
    this.pageSize = size;
    this.initializeData();
  }
}
