import { Component, OnInit } from "@angular/core";
import { GridService } from "./grid.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "custom-grid";
  tableData: any = [];
  isPaginatable: boolean = true;
  pageSize: number = 10;

  constructor(private gridService: GridService) {}

  ngOnInit() {
    this.getJsonData();
  }

  getJsonData() {
    this.gridService.getServiceData().subscribe((response: any) => {
      this.tableData = response.data;
    });
  }
}
